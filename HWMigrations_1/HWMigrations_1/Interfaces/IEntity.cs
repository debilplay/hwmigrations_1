﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HWMigrations_1.Interfaces
{
    public interface IEntity<T>
    {
        public T ID { get; set; }
    }
}
