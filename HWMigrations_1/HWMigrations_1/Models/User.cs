﻿using HWMigrations_1.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace HWMigrations_1.Models
{
    public class User : IEntity<int>
    {
        public int ID { get; set; }

        [Required]
        public string Login { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public int DateOfBirth { get; set; }
        public int AcountCreateDate { get; set; }

    }
}
