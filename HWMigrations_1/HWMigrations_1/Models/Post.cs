﻿using HWMigrations_1.Interfaces;
using HWMigrations_1.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace HWMigrations_1.Models
{
    public class Post : IEntity<int>
    {
        public int ID { get; set; }

        [ForeignKey("User")]
        public int UserID { get; set; }
        public string Comment { get; set; }
        public virtual User User { get; set; }
    }
}
