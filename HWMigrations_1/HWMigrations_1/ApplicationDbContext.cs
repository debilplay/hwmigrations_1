﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using HWMigrations_1.Models;

namespace HWMigrations_1
{
    public class ApplicationDbContext : DbContext
    {
        DbSet<Post> Posts { get; set; }
        DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(@"C:\GitRepositories\HWMigrations_1\HWMigrations_1\HWMigrations_1");
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            var connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);

        }
    }
}
